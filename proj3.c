/*
 * Soubor:  proj2.c
 * Datum:   31.10.2012
 * Autor:   David Spilka (xspilk00)
 * Projekt: Øešení osmismšrky, projekt è. 3 pro pøedmìt IZP
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TEST 1       //1 = kód funkce pro kontrolu vstupních dat
#define SEARCH 2       //2 = kód funkce pro hledání výskytu zadaného slova
#define SOLVE 3     //3 = kód funkce pro hledání tajenky
#define HELP 4        //4 = kód funkce pro zobrazení nápovìdy

#define PARM_ERR 1      //chybnì zadaný parametr
#define IN_ERR 2        //chybnì zadaný vstup
#define FOPEN_ERR 3     //cybně otevřený soubor
#define FCLOSE_ERR 4    //chybně zavřený soubor
#define NOTFOUND_ERR 5  //slovo nenalezeno

void printHelp();
void printErr();
void doParams();
void test();
void search();
void sLeftRight();
void sRightLeft();
void sUpDown();
void sDownUp();
void sUpRight();
void sUpLeft();
void sDownRight();
void sDownLeft();
void vypis();
void loadOsm();
void dataMemFree();
void strMemFree();
void solve();

struct KODY {
  unsigned int funkce;      //kod funkce
  unsigned int isError;     //je/není chyba
  unsigned int nalezeno;    //slova nalezeno/nenalezeno
} kod;

typedef struct
{
    int rows;           //řádky osmisměrky
    int cols;           //sloupce osmisměrky
    char **data;        //osmisměrka
    char *str;          //hledané slovo
    char *pom_str;      //pomocná řetězec při hledání slova
    int strlen;         //délka slova
}Tarray2D;

/* Napoveda */

void printHelp(void)
{
  printf("Napoveda k programu:\n"
       "Program: Projekt c. 3 - Reseni osmismerky.\n"
       "Autor: David Spilka (xspilk00)\n"
       "Program hleds slova zadans uzivatelem v osmismerce,\n"
       "take umi osmismerku rovnou vylustit. Osmismerka je zadavana\n"
       "v souboru ve specifickem formatu.\n"
       "Pouziti: proj2 --help\n"
       "         proj2 -h\n"
       "         proj2 --search=slovo osmismerka.txt\n"
       "         proj2 --solve osmismerka.txt slova.txt\n"
       "\n- slovo = slovo hledane v osmismerce\n"
       "Popis parametru:\n"
       "  -h                  Vypise tuto obrazovku s napovedou.\n"
       "  --help              Vypise tuto obrazovku s napovedou.\n"
       "  --search=slovo      Najde vyskyty slova v osmismerce a \n"
       "                      oznaci je.\n"
       "  --solve             Vylusti osmismerku pomoci slov zadanych v souboru.\n"
       "                      Tajenku vytiskne na standardní výstup.\n"
       "Priklad pouziti:\n"
       "  proj2 --solve osm.txt slova.txt\n\n"
       "    Program pomocí slov zadanych v souboru slova.txt vyluzti osmismerku\n"
       "    ze souboru osm.txt. Tajenku pote vypise.\n");
  return;
}

/* Chybová hlá?ení */
//funkce dle obrženého chybového kódu vypíše chybové hlášení a nastaví kód pro chybu běhu v programu
void printErr(int err)
{
  switch(err) {
    case PARM_ERR:
      fprintf(stderr, "\nChyba v zadani parametru!\n");
      kod.isError = 1;
      break;
    case IN_ERR:
      fprintf(stderr, "\nChyba v zadani vstupu!\n");
      kod.isError = 1;
      break;
    case FOPEN_ERR:
      fprintf(stderr, "\nChyba v otevreni souboru!\n");
      kod.isError = 1;
      break;
    case FCLOSE_ERR:
      fprintf(stderr, "\nChyba v zavreni souboru!\n");
      kod.isError = 1;
      break;
    case NOTFOUND_ERR:
      fprintf(stderr, "\nChyba: Slovo nenalezeno!\n");
      kod.isError = 1;
      break;
    default:
      fprintf(stderr, "\nNeznámá chyba! Vse spatne!\n");
      kod.isError = 1;
      break;
  }
  return;
}

/* Zpracování parametrů */
//funkce zpracovává parametry aplikace, nastavuje kód funkce nebo deteluje chybu
void doParams(int argc, char *argv[])
{
  if (argc == 2)
  {
     if ((strcmp("-h", argv[1]) == 0) || (strcmp("--help", argv[1]) == 0))
     {
     kod.funkce = HELP;
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else if (argc == 3)
  {
     if (strcmp("--test", argv[1]) == 0)
     {
        kod.funkce = TEST;
     }
     else if (strstr(argv[1], "--search=") != NULL)
     {
        kod.funkce = SEARCH;
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else if (argc == 4)
  {
     if (strcmp("--solve", argv[1]) == 0)
     {
        kod.funkce = SOLVE;
     }
     else
     {
        printErr(PARM_ERR);
     }
  }
  else
  {
     printErr(PARM_ERR);
  }
  return;
}

/* Test */
//funkce zpracovává soubor s osmisměrkou, převádí do správného formátu, detekuje chyby v zadání osmisměrky
void test(char *argv[], Tarray2D *ARR)
{
     FILE *f;
     if ((f = fopen(argv[2], "r")) == NULL)     //když se nepovede otevřít soubor
     {
            printErr(FOPEN_ERR);                //vypiš chybové hlášení
     }
     else
     {
         if(fscanf(f, "%d %d", &ARR->rows, &ARR->cols) == 2)        //načtení rozměrů
         {
             int i;
             int j;
             char odpad;
             ARR->data = (char **)malloc(sizeof(char *) * ARR->cols);       //alokace paměti
             for(i = 0; i < ARR->rows; i++)
             {
                 ARR->data[i] = (char*)malloc(sizeof(char) * ARR->cols);    //alokace paměti
             }
             //následující příkazy načítají osmisměrku do dvourozměrného pole
             for(i=0; i < ARR->rows; i++)
	         {
                 for(j = 0; j < ARR->cols; j++)
		         {
			         fscanf(f, " %c", &ARR->data[i][j]);
			         if(ARR->data[i][j] == EOF)     //když soubor skončí dřív, než je načteno minimální požadované množství znaků
			         {
                        printErr(IN_ERR);           //tak vypiš chybové hlášení
                        return;                     //a skonči
                     }
			         if (islower(ARR->data[i][j]) != 0)     //když je načtený znak malé písmeno
			         {
                         if(ARR->data[i][j] == 'c')         //když je načtený znak 'c'
                         {
                             fscanf(f, "%c", &odpad);       //podívám se na následující znak bez vynechání bílých znaků
                             if(odpad == 'h')               //pokud je to 'h'
                             {
                                 ARR->data[i][j] = '_';     //tak na místo 'c' uložím náhradní znak
                                 fscanf(f, "%c", &odpad);   //podívám se na další znak bez vynechání bálých znaků
                                 if(isspace(odpad) == 0)    //pokud je to není mezera
                                 {
                                     printErr(IN_ERR);      //tak vypíšu chybu
                                     return;                //a skončím
                                 }
                             }
                             else if(isspace(odpad) == 0)   //pokud bezprostně po 'c' nenaležela mezera ani 'h'
                             {
                                 printErr(IN_ERR);          //vypíšu chybu
                                 return;                    //a skončím
                             }
                         }
                         else
                         {
                             fscanf(f, "%c", &odpad);
                             if(isspace(odpad) == 0)        //když po načteném znaku není mezera
                             {
                                 printErr(IN_ERR);          //vypíšu chybu
                                 return;                    //a skončím
                             }
                         }
                     }
                     else                                   //když načtený znak není malé písmeno
                     {
                         printErr(IN_ERR);                  //vypiš chybu
                         return;                            //a skonči
                     }
                 }
             }
             vypis(ARR);            //výpis správného formátu osmisměrky na výstup
             dataMemFree(ARR);      //uvolnení paměti
         }
         else                       //pokud jsou chybně zadány rozměry osmisměrky
         {
             printErr(IN_ERR);      //vypiš chybu
             return;                //a skonči
         }
         if (fclose(f) == EOF)      //pokud se sobor nepodaří uzavřít
         {
             printErr(FCLOSE_ERR);  //vypiš chybu
             return;                //a skonči
         }
     }
     return;
}

/* Search */
//funkce hledající výskyt daného slova v osmisměrce
void search(char *argv[], Tarray2D *ARR)
{
     unsigned int i, j;
     ARR->pom_str = (char*)malloc(sizeof(char) * (strlen(argv[1]) + 1));    //alokace paměti pomocné proměnné
     strcpy(ARR->pom_str, argv[1]);                                         //zkopírování parametru do pomocné proměnné
     ARR->str = (char*)malloc(sizeof(char) * (strlen(argv[1])-9+1));        //alokace paměti pro hledané slovo
     strcpy(ARR->str, (ARR->pom_str + 9));                                  //zkopírování pouze hledaného slova do proměnné pro slovo
     free(ARR->pom_str);                                                    //uvolnění paměti pomocné proměnné
     for(i = 1; i < (strlen(argv[1]) - 9); i++)                             //následující příkazy nahrazují ve slově 'ch' náhradním znakem
     {
        if(*(ARR->str + i) == 'h' && *(ARR->str + i - 1) == 'c')
        {
            *(ARR->str + i - 1) = '_';
            for(j = 0; j <= (strlen(argv[1]) - 9) -i; j++)
            {
                *(ARR->str + i + j) = *(ARR->str + i + j + 1);
            }
            *(ARR->str + (strlen(argv[1]) - 9)) = '\0';
        }
     }
     loadOsm(argv, ARR);                            //načtení osmisměrky
     if(kod.isError == 1)                           //pokud bylo načtení chybné
     {
        dataMemFree(ARR);                           //uvolni paměť alokovanou pro osmisměrku
        strMemFree(ARR);                            //uvolni paměť alokovanou pro slovo
        return;                                     //a skonči
     }
     ARR->strlen = strlen(ARR->str);                //uložení délky slova
     kod.nalezeno = 0;                              //vynulování kódu pro nalezeno
     //funkce pro vyhlédávání a označení slova v osmisměrce:
     sLeftRight(ARR);
     sRightLeft(ARR);
     sUpDown(ARR);
     sDownUp(ARR);
     sUpRight(ARR);
     sUpLeft(ARR);
     sDownRight(ARR);
     sDownLeft(ARR);
     if(kod.nalezeno == 0)          //pokud zadané slovo nebylo nalezeno
     {
        dataMemFree(ARR);           //uvolni paměť alokovanou pro osmisměrku
        strMemFree(ARR);            //uvolni paměť alokovanou pro slovo
        printErr(NOTFOUND_ERR);     //vytiskni chybové hlášení
        return;                     //a skonči
    }
    return;
}

/* Solve */
//funkce pro vyluštěni osmisměrky pomocí slov zadaných v souboru
void solve(char *argv[], Tarray2D *ARR)
{
    FILE *fwords;
    int maxstr;
    int i, j;
    if ((fwords = fopen(argv[3], "r")) == NULL)     //pokud se soubor se slovy nepodaří otevřít
    {
        dataMemFree(ARR);                           //uvolni paměť alokovanou pro osmisměrku
        printErr(FOPEN_ERR);                        //vypiš chybu
        return;                                     //a skonči
    }

    if(ARR->rows > ARR->cols)                       //když je počet řádku větší jak počet sloupců
        maxstr = ARR->rows;                         //maximální delka slova = počet řádků
    else                                            //pokud počet řádku není větší jak počet sloupců
        maxstr = ARR->cols;                         //maximální delka slova = počet sloupců

    ARR->str = (char*)malloc(sizeof(char) * maxstr + 1);        //alokace paměti pro slovo
    while(fscanf(fwords, "%s", ARR->str) != EOF)                //načítání slov, dokud není konec souboru
    {
        ARR->strlen = strlen(ARR->str);                         //zjištění délky slova
        for(i = 1; i < ARR->strlen; i++)                        //následující příkazy nahrazují ve slově 'ch' náhradním znakem
        {
            if(*(ARR->str + i) == 'h' && *(ARR->str + i - 1) == 'c')
            {
                *(ARR->str + i - 1) = '_';
                for(j = 0; j <= ARR->strlen -i; j++)
                {
                    *(ARR->str + i + j) = *(ARR->str + i + j + 1);
                }
                *(ARR->str + strlen(ARR->str)) = '\0';
            }
        }
        ARR->strlen = strlen(ARR->str);         //uložení délky slova
        //funkce pro vyhlédávání a označení slova v osmisměrce:
        sLeftRight(ARR);
        sRightLeft(ARR);
        sUpDown(ARR);
        sDownUp(ARR);
        sUpRight(ARR);
        sUpLeft(ARR);
        sDownRight(ARR);
        sDownLeft(ARR);
    }
    //výpis nevyškrtaných písmen na výstup:
    for(i = 0; i < ARR->rows; i++)
	{
	   for(j = 0; j < ARR->cols; j++)
	   {
            if(ARR->data[i][j] == '_')
				printf("ch");
            else if(islower(ARR->data[i][j]) != 0)
				printf("%c", ARR->data[i][j]);
       }
    }
    printf("\n");
    dataMemFree(ARR);               //uvolni paměť alokovanou pro osmisměrku
    strMemFree(ARR);                //uvolni paměť alokovanou pro slovo
    if (fclose(fwords) == EOF)      //pokud se soubor se slovy nepodaří zavřít
    {
        printErr(FCLOSE_ERR);       //vypiš chybové hlášení
        return;                     //a skonči
    }
}

/* Načtení osmisměrky */
void loadOsm(char *argv[], Tarray2D *ARR)
{
    FILE *fosm;
    int i, j;
    char odpad;
    if ((fosm = fopen(argv[2], "r")) == NULL)       //pokud soubor s omsisměrkou nelze načíst
    {
        printErr(FOPEN_ERR);                        //vypiš chybu
        return;                                     //a skonči
    }
    fscanf(fosm, "%d %d", &ARR->rows, &ARR->cols);      //načtení rozměrů osmisměrky
    ARR->data = (char **)malloc(sizeof(char *) * ARR->cols);        //alokace paměti pro osmisměrku
    for(i = 0; i < ARR->rows; i++)
    {
        ARR->data[i] = (char*)malloc(sizeof(char) * ARR->cols);     //alokace paměti pro osmisměrku
    }
    for(i=0; i < ARR->rows; i++)                    //následující příkazy nahrazují ve slově 'ch' náhradním znakem
    {
        for(j = 0; j < ARR->cols; j++)
        {
            fscanf(fosm, " %c", &ARR->data[i][j]);
            if(ARR->data[i][j] == 'c')
            {
                fscanf(fosm, "%c", &odpad);
                if(odpad == 'h')
                {
                    ARR->data[i][j] = '_';
                }
            }
        }
    }
    if (fclose(fosm) == EOF)        //pokud soubor s osmisměrkou nelze načíst
    {
        dataMemFree(ARR);           //uvolni paměť alokovanou pro osmisměrku
        printErr(FCLOSE_ERR);       //vypiš chybové hlášení
        return;                     //a skonči
    }
    return;
}

//následných 8 funkcí prohledává osmisměrku ve směrech daných názvi funkcí, funkce porovnává znaky ve slově se znaky (písmena) v osmisměrce, pokud se stejná posloupnost znaků
//najde v daném směru i ve slově, písmena v osmisměrce se zvětší ('_' se zamění za '?'). Porovnávání znaků začne vždy, když se najde počáteční písmeno. Zvětšení se provede
//vždy až po najití celého slova.

void sLeftRight(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = 0; j < ARR->cols; j++)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j+k) < ARR->cols)
                  {
                    if(*(ARR->str+k) == ARR->data[i][j+k]  || (*(ARR->str+k) == '_' && ARR->data[i][j+k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i][j+k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i][j+k] == '_')
                        ARR->data[i][j+k] = '?';
                        else
                        ARR->data[i][j+k] = toupper(ARR->data[i][j+k]);
                    }
                }
            }
        }
    }
    return;
}

void sRightLeft(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = (ARR->cols-1); j != 0; j--)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j-k) >= 0)
                  {
                    if(*(ARR->str+k) == ARR->data[i][j-k] || (*(ARR->str+k) == '_' && ARR->data[i][j-k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i][j-k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i][j-k] == '_')
                        ARR->data[i][j-k] = '?';
                        else
                        ARR->data[i][j-k] = toupper(ARR->data[i][j-k]);
                    }
                }
            }
        }
    }
}

void sUpDown(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->cols; i++)
    {
        for(j = 0; j < ARR->rows; j++)
        {
            if(*(ARR->str) == ARR->data[j][i] || (*(ARR->str) == '_' && ARR->data[j][i] == '?') || *(ARR->str) == tolower(ARR->data[j][i]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j+k) < ARR->rows)
                  {
                    if(*(ARR->str+k) == ARR->data[j+k][i] || (*(ARR->str+k) == '_' && ARR->data[j+k][i] == '?')  || *(ARR->str+k) == tolower(ARR->data[j+k][i]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        kod.nalezeno = 1;
                        if(ARR->data[j+k][i] == '_')
                        ARR->data[j+k][i] = '?';
                        else
                        ARR->data[j+k][i] = toupper(ARR->data[j+k][i]);
                    }
                }
            }
        }
    }
}

void sDownUp(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->cols; i++)
    {
        for(j = (ARR->rows-1); j != 0; j--)
        {
            if(*(ARR->str) == ARR->data[j][i] || (*(ARR->str) == '_' && ARR->data[j][j] == '?') || *(ARR->str) == tolower(ARR->data[j][i]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j-k) >= 0)
                  {
                    if(*(ARR->str+k) == ARR->data[j-k][i] || (*(ARR->str+k) == '_' && ARR->data[j-k][i] == '?')  || *(ARR->str+k) == tolower(ARR->data[j-k][i]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[j-k][i] == '_')
                        ARR->data[j-k][i] = '?';
                        else
                        ARR->data[j-k][i] = toupper(ARR->data[j-k][i]);
                    }
                }
            }
        }
    }
}

void sDownRight(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = 0; j < ARR->cols; j++)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j+k) < ARR->cols && (i+k) < ARR->rows)
                  {
                    if(*(ARR->str+k) == ARR->data[i+k][j+k] || (*(ARR->str+k) == '_' && ARR->data[i+k][j+k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i+k][j+k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i+k][j+k] == '_')
                        ARR->data[i+k][j+k] = '?';
                        else
                        ARR->data[i+k][j+k] = toupper(ARR->data[i+k][j+k]);
                    }
                }
            }
        }
    }
    return;
}

void sDownLeft(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = 0; j < ARR->cols; j++)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j-k) >= 0 && (i+k) < ARR->rows)
                  {
                    if(*(ARR->str+k) == ARR->data[i+k][j-k] || (*(ARR->str+k) == '_' && ARR->data[i+k][j-k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i+k][j-k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i+k][j-k] == '_')
                        ARR->data[i+k][j-k] = '?';
                        else
                        ARR->data[i+k][j-k] = toupper(ARR->data[i+k][j-k]);
                    }
                }
            }
        }
    }
    return;
}

void sUpRight(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = 0; j < ARR->cols; j++)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j+k) < ARR->cols && (i-k) >= 0)
                  {
                    if(*(ARR->str+k) == ARR->data[i-k][j+k] || (*(ARR->str+k) == '_' && ARR->data[i-k][j+k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i-k][j+k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i-k][j+k] == '_')
                        ARR->data[i-k][j+k] = '?';
                        else
                        ARR->data[i-k][j+k] = toupper(ARR->data[i-k][j+k]);
                    }
                }
            }
        }
    }
    return;
}

void sUpLeft(Tarray2D *ARR)
{
    int stejne;
    int i;
    int j;
    int k;
    for(i = 0; i < ARR->rows; i++)
    {
        for(j = 0; j < ARR->cols; j++)
        {
            if(*(ARR->str) == ARR->data[i][j] || (*(ARR->str) == '_' && ARR->data[i][j] == '?') || *(ARR->str) == tolower(ARR->data[i][j]))
            {
                for(k = 1; k < ARR->strlen; k++)
                {
                  if((j-k) >= 0 && (i-k) >= 0)
                  {
                    if(*(ARR->str+k) == ARR->data[i-k][j-k]  || (*(ARR->str+k) == '_' && ARR->data[i-k][j-k] == '?')  || *(ARR->str+k) == tolower(ARR->data[i-k][j-k]))
                    {
                        stejne = 1;
                        continue;
                    }
                    else
                    {
                        stejne = 0;
                        break;
                    }
                  }
                  else
                  {
                    stejne = 0;
                    break;
                  }
                }
                if(stejne == 1)
                {
                    kod.nalezeno = 1;
                    for(k = 0; k < ARR->strlen; k++)
                    {
                        if(ARR->data[i-k][j-k] == '_')
                        ARR->data[i-k][j-k] = '?';
                        else
                        ARR->data[i-k][j-k] = toupper(ARR->data[i-k][j-k]);
                    }
                }
            }
        }
    }
    return;
}

//funkce pro uvolnění paměti alokované pro hledané slovo
void strMemFree(Tarray2D *ARR)
{
    free(ARR->str);
}

//funkce pro uvolnění paměti alokované pro osmisměrku
void dataMemFree(Tarray2D *ARR)
{
    int i;
    int j = ARR->rows;
    for(i = 0; i < j; i++)
    {
        free(ARR->data[i]);
    }
    free(ARR->data);
}

//funkce pro výpis osmisměrky na výstup (funckce serach a test)
void vypis(Tarray2D *ARR)
{
    int i;
    int j;
    printf("%d %d\n", ARR->rows, ARR->cols);
	for(i = 0; i < ARR->rows; i++)
	{
	   for(j = 0; j < ARR->cols; j++)
	   {
            if(ARR->data[i][j] == '_')
            {
                if(j == ARR->cols-1)
				    printf("ch\n");
			    else
				    printf("ch ");
            }
            else if(ARR->data[i][j] == '?')
            {
                if(j == ARR->cols-1)
				    printf("CH\n");
			    else
				    printf("CH ");
            }
            else
            {
                if(j == ARR->cols-1)
				    printf("%c\n", ARR->data[i][j]);
			    else
				    printf("%c ", ARR->data[i][j]);
            }
        }
    }
}

int main(int argc, char *argv[])
{
  kod.isError = 0;
  Tarray2D ARR;
  doParams(argc, argv);     //volání funkce pro zpracování parametrů
  if(kod.funkce == HELP)    //pokud se kód funkce schoduje s kódem funkce nápovědy
  {
    printHelp();            //vytiskni napovědu
  }
  else if(kod.isError == 0) //pokud nenastala žádná chyba
  {
      switch(kod.funkce)
      {
        case TEST:          //pokud se kód funkce schoduje s kódem funkce test
          test(argv, &ARR); //zavolej funkci test
          break;
        case SEARCH:        //pokud se kód funkce schoduje s kódem funkce search
          search(argv, &ARR);   //zavolej funkci search
          if(kod.isError == 1) return 1;    //pokud nastala chyba, skonči
          strMemFree(&ARR);                 //uvolni paměť alokovanou pro hledané slovo
          if(kod.isError == 1) return 1;    //pokud nastala chyba, skonči
          vypis(&ARR);                      //vypiš osmisměrku
          dataMemFree(&ARR);                //uvolni paměť alokovanou pro osmisěrku
          break;
        case SOLVE:         //pokud se kód funkce schoduje s kódem funkce solve
          loadOsm(argv, &ARR);          //načti osmisměrku
          if(kod.isError == 0)          //pokud nenastala chyba
          {
            solve(argv, &ARR);          //zavolej funkci solve
          }
          break;
        default:
          break;
      }
  }
  return 0;
}
