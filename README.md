﻿# Základy programování - 3. projekt - Řešení osmisměrky #


## Hodnocení ##


6/10


## Chyby ##

ad funkcnost:
-0.5b search funguje pouze částečně - nenajde všechny výskyty (směr nahoru při slově s CH).

ad implementace:
-1.5b: CHYBA: POUZIVA scanf.*%s - co když Vám uživatel zadá slovo délky 10* větší rozměr matice?
  - deklarace funkcí louží k tomu, aby překladač věděl, jaké budou mít funkce parametry a návratové typy
  - návratové hodnoty jste mohl využít pro detekci chyb
-1b Program netestuje, jestli se alokace paměti povedla. Po každém volání malloc, realloc, atd. je nutné otestovat, zda tyto funkce nevrátily NULL.
  - Program není dostatečně komentovaný. Hlavičky funkcí musí být komentovány včetně popisu parametrů.
-1b Opakování stejného kódu namísto vícenásobného volání jediné funkce. Nedostatečně zobecněný algoritmus. viz 4.6 Rozkopírovaný kód